#!/usr/bin/env sh

CURRENT_DIR=$(dirname $(readlink -f "$0"))

version=$(lsb_release -sr)

sudo cp -f $CURRENT_DIR/../configs/timezone /etc/timezone
sudo cp -f $CURRENT_DIR/../configs/locale.gen /etc/locale.gen
sudo cp -f $CURRENT_DIR/../configs/locale /etc/default/locale

sudo dpkg-reconfigure -frontend=noninteractive tzdata
sudo dpkg-reconfigure -frontend=noninteractive locales

sudo update-locale LANG=en_US.UTF-8

sudo add-apt-repository ppa:jonathonf/vim -y
sudo apt update
sudo apt install -y git curl vim zsh tmux ncdu htop sshfs python3 build-essential cmake python-dev python3-dev
sudo apt upgrade -y

curl 'http://vim-bootstrap.com/generate.vim' --data 'langs=python&langs=c&langs=html&langs=php&langs=ruby&langs=javascript&editor=vim' | tee $HOME/.vimrc

git clone https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm

cp $CURRENT_DIR/../configs/nanorc.conf $HOME/.nanorc
cp $CURRENT_DIR/../configs/tmux.conf $HOME/.tmux.conf
cp $CURRENT_DIR/../configs/git.conf $HOME/.gitconfig
cp $CURRENT_DIR/../configs/vimrc.local.conf $HOME/.vimrc.local
cp $CURRENT_DIR/../configs/vimrc.local.bundles.conf $HOME/.vimrc.local.bundles

echo "source $HOME/.dotfiles/zsh/init.zsh" | tee $HOME/.zshrc

echo "
ClientAliveInterval 120
ClientAliveCountMax 2
PasswordAuthentication no
GatewayPorts yes" | sudo tee -a /etc/ssh/sshd_config

sudo systemctl restart sshd.service

if [ ! -f /swapfile ]; then
  sudo dd if=/dev/zero of=/swapfile bs=1024 count=2M
  sudo chmod 600 /swapfile
  sudo mkswap /swapfile
  sudo swapon /swapfile
  echo "\n/swapfile       none    swap    sw      0       0" | sudo tee -a /etc/fstab
  echo "10" | sudo tee /proc/sys/vm/swappiness
  echo "vm.swappiness = 10" | sudo tee -a /etc/sysctl.conf
fi

update-alternatives --set python /usr/bin/python3

vim +PlugInstall +PlugClean +qall
sudo python $HOME/.vim/plugged/YouCompleteMe/install.py --clang-completer

mkdir $HOME/.vim/tmp $HOME/.vim/session

echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf

wget https://raw.githubusercontent.com/thestinger/termite/master/termite.terminfo
mv termite.terminfo .termite.terminfo
tic -x .termite.terminfo

chsh -s $(grep /zsh$ /etc/shells | tail -1)
