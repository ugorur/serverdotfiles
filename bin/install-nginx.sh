#!/usr/bin/env sh

CURRENT_DIR=$(dirname $(readlink -f "$0"))

sudo add-apt-repository ppa:certbot/certbot -y
sudo apt install nginx python-certbot-nginx
