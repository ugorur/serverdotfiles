CURRENT_DIR=$(dirname $(readlink -f "$0"))

export TERM=xterm-256color

export DOTFILES_DIR=$(dirname $CURRENT_DIR)
export PROJECTS_HOME=$HOME/Projects
export EDITOR=vim

export PATH="$DOTFILES_DIR/bin:$PATH"
