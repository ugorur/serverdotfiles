# Upgrade alias
alias upgrade="sudo apt update && sudo apt upgrade -y"

# Colorfull LS
alias hal="ls -hal"

# My Git Aliasses
alias gs="git status"
alias gittirgit="git add . && git commit && git push"

# Network Aliasses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"

# Copy Aliasses
alias cprpf="cp -rpf"

# SSL
alias newssl="sudo certbot --authenticator standalone --installer nginx --pre-hook \"systemctl stop nginx.service\" --post-hook \"systemctl start nginx.service\""
