CURRENT_DIR=$(dirname $(readlink -f "$0"))

source $CURRENT_DIR/variables.zsh

source $CURRENT_DIR/antigen.zsh
source $CURRENT_DIR/packages.zsh

source $CURRENT_DIR/aliases.zsh
source $CURRENT_DIR/functions.zsh
