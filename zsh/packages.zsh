antigen use oh-my-zsh

antigen bundle git
antigen bundle pip
antigen bundle docker
antigen bundle yarn
antigen bundle command-not-found
antigen bundle docker-compose
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle djui/alias-tips
#antigen bundle zpm-zsh/autoenv
antigen bundle arzzen/calc.plugin.zsh
antigen bundle RobSis/zsh-completion-generator
antigen bundle voronkovich/gitignore.plugin.zsh
antigen bundle gko/project
antigen bundle lukechilds/zsh-nvm
antigen bundle zsh-users/zsh-autosuggestions

antigen theme bira

antigen apply
